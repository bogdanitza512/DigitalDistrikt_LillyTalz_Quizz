﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ScreenMgr;

/// <summary>
/// 
/// </summary>
[ExecuteInEditMode]
public class CallToActionScreenManager : AnimatorScreen
{

    #region Fields and Properties

    [SerializeField]
    GameObject sphere;

    [SerializeField]
    bool isActive;

    [SerializeField]
    bool lastIsActive;

    [SerializeField]
    Material dissolveMaterial;

    [SerializeField]
    string propertyName = "Vector1_68C5C62B";

    [Range(0.0f, 1.0f)]
    public float dissolveAmount;

    [SerializeField]
    float lastDissolveAmount = 1.0f;

    [SerializeField]
    float deltaEpsilon = 0.01f;

    /*
    {
        get
        {
            return dissolveMaterial.GetFloat(propertyName);
        }

        set
        {
            dissolveMaterial.SetFloat(propertyName, value);
        }
    }
    */

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        dissolveMaterial.SetFloat(propertyName, lastDissolveAmount);
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if(System.Math.Abs(dissolveAmount - lastDissolveAmount) > deltaEpsilon)
        {
            dissolveMaterial.SetFloat(propertyName, dissolveAmount);
            //print(domeRenderer.GetFloat(propertyName));
            lastDissolveAmount = dissolveAmount;
        }

        if(isActive != lastIsActive)
        {
            sphere.SetActive(isActive);
            lastIsActive = isActive;
        }

    }

    #endregion

    #region Methods

	

    #endregion
}
