﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.Extensions;
using DG.Tweening;
using ScreenMgr;

/// <summary>
/// 
/// </summary>
public class EndScreenManager : AnimatorScreen
{

    #region Fields and Properties

    [SerializeField]
    TMP_Text text;

    [SerializeField]
    string template;

    [SerializeField]
    GameMode gameMode;

    [SerializeField]
    float restartDelay = 2.5f;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void OnScoreChanged(int score)
    {
        text.text = template.FormatWith(score);
    }

    public void WaitForRestart()
    {
        DOTween.Sequence()
               .AppendInterval(restartDelay)
               .OnComplete(gameMode.RestartScene);
    }


    #endregion

}
