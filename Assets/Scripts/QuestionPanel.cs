﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
[ExecuteInEditMode]
public class QuestionPanel : SerializedMonoBehaviour
{

    #region Nested Types

    public enum State { On, Off }

    public struct Specs
    {
        public float fillAmount;
        public Color color;
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    State currentState = State.Off;

    [SerializeField]
    Dictionary<State, Specs> stateSpecsDict
        = new Dictionary<State, Specs>()
    {
        {State.On, new Specs(){ fillAmount = 1.0f, color = Color.white }},
        {State.Off, new Specs(){ fillAmount = 0.0f, color = Color.gray }}
    };

    [SerializeField]
    float duration = 2.5f;

    [SerializeField]
    Image image;

    [SerializeField]
    TMP_Text text;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

	public void SetState(State _state)
    {
        if (currentState != _state)
        {
            currentState = _state;
            DOTween.Sequence()
                   .Append(DOTween.To(() => image.fillAmount,
                                      (x) => image.fillAmount = x,
                                      stateSpecsDict[_state].fillAmount,
                                      duration))
                   .Join(DOTween.To(() => text.color,
                                    (x) => text.color = x,
                                    stateSpecsDict[_state].color,
                                    duration));

        }
    }

    [Button]
    void ToggleStates()
    {
        switch(currentState)
        {
            case State.Off:
                SetState(State.On);
                break;
            case State.On:
                SetState(State.Off);
                break;
        }
    }

    #endregion
}
