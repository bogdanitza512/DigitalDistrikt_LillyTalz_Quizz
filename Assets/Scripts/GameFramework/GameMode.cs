﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Extensions;
using System;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class GameMode : SerializedMonoBehaviour
{

    #region Nested Types

    public enum Type { Client }

    [Serializable]
    public struct Specs
    {

        /// <summary>
        /// The target screen resolution.
        /// </summary>
        [Tooltip("Target screen resolution.")]
        public Vector2Int targetResolution;

    }

    #endregion

    #region Fields and Properties

    /// <summary>
    /// The player.
    /// </summary>
    [Tooltip("The player.")]
    public Type currentGameMode;

    /// <summary>
    /// The game mode specs map.
    /// </summary>
    [Tooltip("The game mode specs map.")]
    public Dictionary<Type, Specs> specsMap
        = new Dictionary<Type, Specs>()
    {{Type.Client, new Specs(){targetResolution = new Vector2Int(1920,1080)}}};

    /// <summary>
    /// The target screen mode.
    /// </summary>
    [SerializeField]
    [Tooltip("The target screen mode.")]
    FullScreenMode targetScreenMode = FullScreenMode.FullScreenWindow;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        SetupPlayerScreen(currentGameMode);
    }



    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
    }

    #endregion

    #region Methods

    /// <summary>
    /// Setups the player screen.
    /// </summary>
    /// <param name="gameModeType">Type of the GameMode.</param>
    private void SetupPlayerScreen(Type gameModeType)
    {
        var resolution = specsMap[gameModeType]
                                 .targetResolution;
        Screen.SetResolution(resolution.x,
                             resolution.y,
                             targetScreenMode);
    }

    /// <summary>
    /// Restarts the scene.
    /// </summary>
    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    /// <summary>
    /// Quits the game.
    /// </summary>
    public void QuitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
        print("Unity.Editor::Playmode stopped by {0}".FormatWith(this.name));
#else
        Application.Quit();
#endif
    }



    #endregion
}
