﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using ScreenMgr;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class QuizzScreenManager : AnimatorScreen
{

    #region Nested Types

    [System.Serializable]
    public class UnityEventIntChanged : UnityEvent<int>
    {
    }


    #endregion

    #region Fields and Properties

    public int currentScore;

    public int Score
    {
        get { return currentScore; }

        set { currentScore = value; onScoreChanged.Invoke(value); }
    }

    public int answearedQuestions = 0;

    public float goalReachedDelay = 2.5f;

    public int goal = 10;

    public int AnswearedQuestions
    {
        get{
            return answearedQuestions;
        }

        set{
            answearedQuestions = value;
            if (answearedQuestions >= goal)
                DOTween.Sequence()
                       .AppendInterval(goalReachedDelay)
                       .OnComplete(onGoalReached.Invoke);
        }
    }

    public ScreenManager questionManager;

    public UnityEventIntChanged onScoreChanged;

    public UnityEvent onShowScreen;

    public UnityEvent onGoalReached;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public override void OnShow()
    {
        base.OnShow();
        onShowScreen.Invoke();
    }

    public void OnSendAnswearButton_Cliked()
    {
        questionManager.Current.gameObject.GetComponent<QuestionResolver>().ResolveAnswear();
    }

    #endregion

}
