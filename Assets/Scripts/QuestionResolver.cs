﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class QuestionResolver : SerializedMonoBehaviour
{

    #region Nested Types

    public enum Variant { A, B, C, D }
    public struct VariantContent
    {
        public string text;
        public bool isCorrect;
    }


    #endregion

    #region Fields and Properties

    [SerializeField]
    QuizzScreenManager scoreManager;

    [SerializeField]
    float delayDuration = 2.5f;

    [SerializeField]
    List<AnswearVariant> variants 
        = new List<AnswearVariant>();

    public UnityEvent onComputeAnswear;

    public UnityEvent onAnswearComputed;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    bool hasBeenResolved = false;

    public void ResolveAnswear()
    {
        if (hasBeenResolved == true) return;

        DOTween.Sequence()
               .OnStart(ComputeVariantsValidity)
               .AppendInterval(delayDuration)
               .OnComplete(onAnswearComputed.Invoke);
        
        hasBeenResolved = true;
    }

    void ComputeVariantsValidity()
    {
        onComputeAnswear.Invoke();

        bool validAns = true;
        foreach (var variant in variants)
        {
            validAns &= variant.ComputeValidity();
        }

        scoreManager.Score += validAns ? 1 : 0;

    }

    #endregion
}
