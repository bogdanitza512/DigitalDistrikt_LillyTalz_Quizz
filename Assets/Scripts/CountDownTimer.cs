﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.Events;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class CountDownTimer : MonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    int duration = 180;

    [SerializeField]
    Image progressBar;

    [SerializeField]
    TMP_Text displayText;

    [SerializeField]
    int currentTime = 180;

    public UnityEvent onCountDownFinished;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// This function is called when the object becomes enabled and active
    /// </summary>
    private void OnEnable()
    {
        //StartCounter();
    }

    /// <summary>
    /// This function is called when the behaviour becomes disabled or inactive
    /// </summary>
    private void OnDisable()
    {
        countdownTimerSeq.Kill();
    }



    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    Sequence countdownTimerSeq;

    public void Play()
    {
        countdownTimerSeq = 
        DOTween.Sequence()
               .Append(progressBar.DOFillAmount(0, duration)
                                  .SetEase(Ease.Linear))
               .Join(DOTween.To(() => { return currentTime; },
                                (x) => { UpdateDisplay(x); },
                                0,
                                duration)
                            .SetEase(Ease.Linear))
                            .OnStart(() => { UpdateDisplay(duration); })
               .OnComplete(() => { onCountDownFinished.Invoke(); });
    }

    public void UpdateDisplay(int time)
    {
        if(currentTime != time)
        {
            currentTime = time;
            displayText.text = time.ToString();
        }
    }

    [Button(ButtonSizes.Medium)]
    public void UpdateCounter()
    {
        currentTime = duration;
        displayText.text = duration.ToString();
    }

    public void Pause()
    {
        //DOTween.KillAll();
        countdownTimerSeq.Pause();
    }

    #endregion

}
