﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class LevelManager : MonoBehaviour
{

    #region Fields and Properties

    [SerializeField]
    int _currentIndex = -1;

    public int currentIndex
    {
        set
        {
            _currentIndex = value > (questionPanels.Count-1)? 
                questionPanels.Count-1 : value;
            for (int i = 0; i <= _currentIndex; i++)
            {
                questionPanels[i].SetState(QuestionPanel.State.On);
            }
        }
        get{
            return _currentIndex;
        }
    }


    [SerializeField]
    List<QuestionPanel> questionPanels;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    [Button]
    public void IncrementLevelIndex()
    {
        currentIndex++;
    }

    #endregion
}
