﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using Sirenix.OdinInspector;

/// <summary>
/// 
/// </summary>
public class AnswearVariant : SerializedMonoBehaviour
{

    #region Nested Types

    public enum ValidityState { Uncertain, Right, Wrong };

    public enum UseState { Unselected, Selected };

    public struct SuperState
    {
        public UseState useState;

        public ValidityState validity;

        public SuperState(UseState _useState,
                          ValidityState _validity)
        {
            useState = _useState;
            validity = _validity;
        }
    }

    public struct TransitionSpecs
    {
        
        public Color buttonColor;

        public Color textColor;

        public float durationMultiplier;

        public int loops;
    
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    ValidityState targetValidityState = ValidityState.Uncertain;

    [SerializeField]
    Button variantButton;

    [SerializeField]
    TMP_Text variantText;

    [SerializeField]
    UseState currentUseState = UseState.Unselected;

    [SerializeField]
    ValidityState currentValidityState = ValidityState.Uncertain;

    [SerializeField]
    float baseDuration = 0.5f;

    [SerializeField]
    Dictionary<SuperState, TransitionSpecs> colorDict
    = new Dictionary<SuperState, TransitionSpecs>();

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
       
    }

    #endregion

    #region Methods

    public void OnVariantButton_Click()
    {
        TransitionToState(currentUseState == UseState.Selected ?
                                                      UseState.Unselected :
                                                      UseState.Selected);
        // quizzManager.NotifyStateChange(currentState);
    }

    public void TransitionToState(UseState _userState)
    {
        if(currentUseState != _userState)
        {
            TransitionToState(_userState, currentValidityState);
            currentUseState = _userState;
        }
        //buttonImage.DOColor(variantColorMap[state], transitionDuration);


    }

    public void TransitionToState(ValidityState _validityState)
    {
        if (currentValidityState != _validityState)
        {
            TransitionToState(currentUseState, _validityState);
            currentValidityState = _validityState;
        }

    }

    public void TransitionToState(UseState _useState, ValidityState _validityState)
    {
        var specs = colorDict[new SuperState(_useState, _validityState)];
        var duration = baseDuration * specs.durationMultiplier;

        DOTween.Sequence()
               .Append(variantButton.image.DOColor(specs.buttonColor, duration)
                                          .SetLoops(specs.loops, LoopType.Yoyo))
               .Join(DOTween.To(() => { return variantText.color; },
                                (x) => { variantText.color = x; },
                                specs.textColor, duration));

        
    }

    public bool ComputeValidity()
    {

        variantButton.interactable = false;

        bool validity = 
            (currentUseState == UseState.Selected &&
              targetValidityState == ValidityState.Right) ||
            (currentUseState == UseState.Unselected &&
                targetValidityState == ValidityState.Wrong);

        TransitionToState(targetValidityState);

        return validity;
    }

    #endregion
}
