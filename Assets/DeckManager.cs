﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class DeckManager : MonoBehaviour
{

    #region Fields and Properties

    public List<DeckCardComponent> cards;

    public DeckQAResolver currentDeckResolver;

    public bool areAnyCardsFacingUp;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void SetCurrentResolver(DeckQAResolver resolver)
    {
        currentDeckResolver = resolver;
    }

    public void SolveCurrentResolver()
    {
        currentDeckResolver.SolveAnswearValidity();
    }

    #endregion
}
