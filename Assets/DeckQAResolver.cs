﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class DeckQAResolver : SerializedMonoBehaviour
{

    #region Nested Types

    public enum Orientation { FaceDown, FaceUp }

    public enum ValidityState { Uncertain, Right, Wrong };

    public enum UseState { Unselected, Selected };

    public struct SuperState
    {
        public UseState useState;

        public ValidityState validity;

        public SuperState(UseState _useState,
                          ValidityState _validity)
        {
            useState = _useState;
            validity = _validity;
        }
    }

    public struct TransitionSpecs
    {
        public Color textColor;

        public float durationMultiplier;

        public int loops;
    }

    #endregion

    #region Fields and Properties

    [SerializeField]
    QuizzScreenManager scoreManager;

    [SerializeField]
    float delayDuration = 2.5f;

    private bool hasBeenResolved;

    public List<DeckQAVariant> variants
        = new List<DeckQAVariant>();


    public Dictionary<SuperState, TransitionSpecs> colorDict
         = new Dictionary<SuperState, TransitionSpecs>()
    {
        {new SuperState(UseState.Unselected,ValidityState.Uncertain), new TransitionSpecs()},
        {new SuperState(UseState.Unselected,ValidityState.Right), new TransitionSpecs()},
        {new SuperState(UseState.Unselected,ValidityState.Wrong), new TransitionSpecs()},
        {new SuperState(UseState.Selected,ValidityState.Uncertain), new TransitionSpecs()},
        {new SuperState(UseState.Selected,ValidityState.Right), new TransitionSpecs()},
        {new SuperState(UseState.Selected,ValidityState.Wrong), new TransitionSpecs()}
    };

    public UnityEvent onComputeAnswear;

    public UnityEvent onAnswearComputed;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    public void SolveAnswearValidity()
    {
        if (hasBeenResolved == true) return;

        scoreManager.AnswearedQuestions++;

        DOTween.Sequence()
               .OnStart(ComputeVariantsValidity)
               .AppendInterval(delayDuration)
               .OnComplete(onAnswearComputed.Invoke);

        hasBeenResolved = true;
    }

    void ComputeVariantsValidity()
    {
        onComputeAnswear.Invoke();

        bool validAns = true;
        foreach (var variant in variants)
        {
            validAns &= variant.ComputeValidity();
        }

        scoreManager.Score += validAns ? 1 : 0;

    }

    #endregion

}