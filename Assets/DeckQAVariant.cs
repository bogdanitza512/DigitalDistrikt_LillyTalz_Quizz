﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using System;
using TMPro;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class DeckQAVariant : SerializedMonoBehaviour
{
    
    #region Fields and Properties

    public DeckQAResolver resolver;

    [SerializeField]
    DeckQAResolver.ValidityState targetValidityState 
                  = DeckQAResolver.ValidityState.Uncertain;

    [SerializeField]
    TMP_Text variantText;

    [SerializeField]
    DeckQAResolver.UseState currentUseState 
                  = DeckQAResolver.UseState.Unselected;

    [SerializeField]
    DeckQAResolver.ValidityState currentValidityState
                  = DeckQAResolver.ValidityState.Uncertain;

    [SerializeField]
    float baseDuration = 0.5f;


    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void PopulateData()
    {
        if (resolver != null &&
            !resolver.variants.Contains(this))
            resolver.variants.Add(this);

        if(variantText == null)
        {
            variantText = GetComponent<TMP_Text>();
        }
    }

    public void OnVariant_Clicked()
    {
        TransitionToState(currentUseState == DeckQAResolver.UseState.Selected ?
                          DeckQAResolver.UseState.Unselected :
                          DeckQAResolver.UseState.Selected);
        // quizzManager.NotifyStateChange(currentState);
    }

    public void TransitionToState(DeckQAResolver.UseState _userState)
    {
        if (currentUseState != _userState)
        {
            TransitionToState(_userState, currentValidityState);
            currentUseState = _userState;
        }
        //buttonImage.DOColor(variantColorMap[state], transitionDuration);


    }

    public void TransitionToState(DeckQAResolver.ValidityState _validityState)
    {
        if (currentValidityState != _validityState)
        {
            TransitionToState(currentUseState, _validityState);
            currentValidityState = _validityState;
        }

    }

    public void TransitionToState(DeckQAResolver.UseState _useState,
                                  DeckQAResolver.ValidityState _validityState)
    {
        var specs = resolver.colorDict[new DeckQAResolver.SuperState(_useState, _validityState)];
        var duration = baseDuration * specs.durationMultiplier;

        DOTween.Sequence()
               .Append(DOTween.To(() => { return variantText.color; },
                                (x) => { variantText.color = x; },
                                        specs.textColor, duration));
    }

    public bool ComputeValidity()
    {

        bool validity =
            (currentUseState == DeckQAResolver.UseState.Selected &&
             targetValidityState == DeckQAResolver.ValidityState.Right) ||
            (currentUseState == DeckQAResolver.UseState.Unselected &&
             targetValidityState == DeckQAResolver.ValidityState.Wrong);

        TransitionToState(targetValidityState);

        return validity;
    }

    #endregion

}