﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine.Events;
using System;
using DG.Tweening;

/// <summary>
/// 
/// </summary>
public class DeckCardComponent : SerializedMonoBehaviour
{


    #region Nested Types

    public enum Orientation { FaceDown, FaceUp }

    public enum Type { Question, Answear }

    #endregion

    #region Fields and Properties

    public Type type = Type.Question;

    public Renderer cardRenderer;

    public string shaderPropertyName = "Vector1_68C5C62B";

    [SerializeField]
    float dissolveAmount = 0.01f;

    float deltaEpsilon = 0.01f;

    float DissolveAmount
    {
        get{
            return dissolveAmount;
        }

        set{
            if (Math.Abs(value - dissolveAmount) > deltaEpsilon)
            {
                dissolveAmount = value;
                cardRenderer.material.SetFloat(shaderPropertyName, value);
            }


        }
    }

    public DeckManager deck;

    public DeckCardComponent match;

    public Orientation orientation = Orientation.FaceDown;

    public Dictionary<Orientation, Transform> orientationDict
    = new Dictionary<Orientation, Transform>()
    {
        {Orientation.FaceDown,null},
        {Orientation.FaceUp,null}
    };

    [SerializeField]
    float duration = 1.0f;

    public bool isInteractible = true;

    public UnityEvent onTurnedFaceDown;

    public UnityEvent onTurnedFaceUp;

    [SerializeField]
    float dissolveDuration;

    #endregion

    #region Unity Messages

    /// <summary>
    /// Start is called just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {

    }

    #endregion

    #region Methods

    [Button(ButtonSizes.Medium)]
    public void PopulateData()
    {
        if(match != null)
        {
            match.match = this;
        }

        if(deck != null && !deck.cards.Contains(this))
        {
            deck.cards.Add(this);
        }

    }

    public void OnDeckCard_Pressed()
    {
        var newOrientation =
            (orientation == Orientation.FaceUp)
            ? Orientation.FaceDown : Orientation.FaceUp;
        if (type == Type.Answear && orientation == Orientation.FaceUp)
            return;

        TurnOver(newOrientation);
    }

    public void TurnOver(Orientation newOrientation)
    {

        if (orientation == newOrientation)
            return;

        if (newOrientation == Orientation.FaceUp &&
            deck.areAnyCardsFacingUp == true)
            return;
       
        if (!isInteractible &&
            newOrientation == Orientation.FaceUp)
            return;

        DOTween.Sequence()
               .Append(transform.DOMove(orientationDict[newOrientation].position,
                                        duration))
               .Join(transform.DORotate(orientationDict[newOrientation].rotation.eulerAngles,
                                          duration))
               .OnComplete(() => CheckFaceState(newOrientation));

        orientation = newOrientation;

        deck.areAnyCardsFacingUp
            = (newOrientation == Orientation.FaceUp);

        match.TurnOver(newOrientation);



    }

    public void TurnOver(int newOrientation)
    {
        TurnOver((Orientation)newOrientation);
    }

    public void CheckFaceState(Orientation newOrientation)
    {
        switch(newOrientation)
        {
            case Orientation.FaceDown:
                onTurnedFaceDown.Invoke();
                break;
            case Orientation.FaceUp:
                onTurnedFaceUp.Invoke();
                break;
        }
    }

    bool hasBeenDissolved = false;

    [Button]
    public void DissolveCard()
    {
        hasBeenDissolved = true;

        /*

        DOTween.Sequence()
               .Append(transform.DOMove(orientationDict[Orientation.FaceDown].position,
                                        duration))
               .Join(transform.DORotate(orientationDict[Orientation.FaceDown].rotation.eulerAngles,
                                          duration))
               .Join(DOTween.To(() => { return DissolveAmount; },
                                (x) => { DissolveAmount = x; },
                                1.0f,
                                dissolveDuration))
               .OnComplete(() => gameObject.SetActive(false));


        */

        DOTween.Sequence()
               .OnStart(() => TurnOver(Orientation.FaceDown))
               .Append(DOTween.To(() => { return DissolveAmount; },
                                  (x) => { DissolveAmount = x; },
                                  1.0f,dissolveDuration))
               .OnComplete( () => { gameObject.SetActive(false); } );

       if (!match.hasBeenDissolved)
            match.DissolveCard();

    }


    #endregion

}
